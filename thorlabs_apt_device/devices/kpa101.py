# Copyright 2023 imyxh
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: this was pretty quickly hacked together and is largely untested. if
# you're using this please report bugs, or---if you are so daring---fix them
# yourself :)

__all__ = ["KPA101"]

from .. import protocol as apt
from .aptdevice import APTDevice
from ..enums import EndPoint

import logging
logging.basicConfig(level=logging.DEBUG)


class KPA101(APTDevice):
    """
    A class specific to the Thorlabs KPA101.

    :param serial_port: Serial port device the device is connected to.
    :param vid: Numerical USB vendor ID to match.
    :param pid: Numerical USB product ID to match.
    :param manufacturer: Regular expression to match to a device manufacturer string.
    :param product: Regular expression to match to a device product string.
    :param serial_number: Regular expression to match to a device serial number.
    :param location: Regular expression to match to a device bus location.
    :param status_updates: Set to ``"auto"``, ``"polled"`` or ``"none"``.
    :param controller: The destination :class:`EndPoint <thorlabs_apt_device.enums.EndPoint>` for the controller.
    :param bays: Tuple of :class:`EndPoint <thorlabs_apt_device.enums.EndPoint>`\\ (s) for the populated controller bays.
    :param channels: Tuple of indices (1-based) for the controller bay's channels.
    """


    def __init__(self,
        serial_port=None, vid=None, pid=None,
        manufacturer=None, product=None, serial_number=69, location=None,
        status_updates="none", controller=EndPoint.USB,
        bays=(EndPoint.USB,), channels=(None,)
    ):

        super().__init__(
            serial_port=serial_port, vid=vid, pid=pid,
            manufacturer=manufacturer, product=product,
            serial_number=serial_number,
            location=location,
            status_updates=status_updates, controller=controller,
            bays=bays, channels=channels,
        )

        self.keepalive_message = apt.quad_ack_statusupdate

        self.loopparams_ = [[{
            "p": 0.0, "i": 0.0, "d": 0.0,
            "low_pass_cutoff": 0.0,
            "notch_center": 0.0,
            "filter_q": 0.0,
            "notch_filter_on": 0,
            "deriv_filter_on": 0,
            "x_diff": 0, "y_diff": 0,
            "sum": 0,
            "x_pos": 0, "y_pos": 0,
            "x_pos_dem_min": 0, "y_pos_dem_min": 0,
            "x_pos_dem_max": 0, "y_pos_dem_max": 0,
            "lv_out_route": 0,
            "ol_pos_dem": 0,
            "x_pos_fb_sense": 0,
            "y_pos_fb_sense": 0,
            "mode": 3,
            "disp_intensity": 0,
            "disp_mode": 0,
            "disp_dim_timeout": 0,
            "trig1_mode": 0,
            "trig1_polarity": 0,
            "trig1_sum_min": 0, "trig1_sum_max": 0,
            "trig1_diff_threshold": 0,
            "trig2_mode": 0,
            "trig2_polarity": 0,
            "trig2_sum_min": 0, "trig2_sum_max": 0,
            "trig2_diff_threshold": 0,
            "dig_outs": 0,
            # Update message fields
            "msg" : "",
            "msgid" : 0,
            "source" : 0,
            "dest" : 0,
        } for _ in self.channels] for _ in self.bays]
        """
        Array of dictionaries of loop parameters.

        As a device may have multiple card bays, each with multiple channels, this data structure
        is an array of array of dicts. The first axis of the array indexes the bay, the second
        indexes the channel.
        """
        # Request current loop parameters
        for bay in self.bays:
            for channel in self.channels:
                self._loop.call_soon_threadsafe(
                    self._write,
                    apt.quad_req_loopparams2(source=EndPoint.HOST, dest=bay)
                )


    def close(self):
        """
        Stop the device and close the serial connection to the ThorLabs APT controller.
        """
        super().close()


    def _process_message(self, m):
        super()._process_message(m)

        # Decode bay and channel IDs and check if they match one of ours
        if m.msg in ("quad_get_params", "quad_get_statusupdate"):
            if m.source == EndPoint.USB:
                # Map USB controller endpoint to first bay
                bay_i = 0
            else:
                # Check if source matches one of our bays
                try:
                    bay_i = self.bays.index(m.source)
                except ValueError:
                    # Ignore message from unknown bay id
                    if not m.source == 0:
                        # Some devices return zero as source of move_completed etc
                        self._log.warn(f"Message {m.msg} has unrecognised source={m.source}.")
                    bay_i = 0
                    #return
            # I don't think the quad detector has channels, so we should always
            # just have channel_i = 0
            channel_i = self.channels.index(None)

        # Act on each message type
        match m.msg:
            case "quad_get_params":
                self.loopparams_[bay_i][channel_i].update(m._asdict())
            case "quad_get_statusupdate":
                self.loopparams_[bay_i][channel_i].update(m._asdict())
            case _:
                #self._log.debug(f"Received message (unhandled): {m}")
                pass


    def set_loopparams(self,
        p: float, i: float, d: float,
        low_pass_cutoff: float = 0,
        notch_center: float = 0,
        filter_q: float = 0,
        notch_filter_on: int = 0,
        deriv_filter_on: int = 0,
        bay=0, channel=0
    ):
        """
        Set the quad detector's loop parameters.

        :param bay: Index (0-based) of controller bay to send the command.
        :param channel: Index (0-based) of controller bay channel to send the command.
        """
        # write params
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_set_loopparams2(
                source=EndPoint.HOST, dest=self.bays[bay],
                p=p, i=i, d=d,
                low_pass_cutoff=low_pass_cutoff,
                notch_center=notch_center,
                filter_q=filter_q,
                notch_filter_on=notch_filter_on,
                deriv_filter_on=deriv_filter_on,
            )
        )
        # request new params to update the dictionary
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_req_loopparams2(source=EndPoint.HOST, dest=self.bays[bay])
        )


    def set_posdemparams(self,
        x_pos_dem_min: -32767,
        y_pos_dem_min: -32767,
        x_pos_dem_max: 32767,
        y_pos_dem_max: 32767,
        lv_out_route: 1,
        ol_pos_dem: 2,
        x_pos_fb_sense: 32767,
        y_pos_fb_sense: 32767,
        bay=0, channel=0
    ):
        """
        Set the quad detector's position-demand parameters.

        :param x_pos_dem_min: Minimum x voltage, where -32768 is -10 V.
        :param y_pos_dem_min: Minimum y voltage, where -32768 is -10 V.
        :param x_pos_dem_max: Maximum x voltage, where 32767 is 10 V.
        :param y_pos_dem_max: Maximum y voltage, where 32767 is 10 V.
        :param lv_out_route: 1 to only output to SMA, 2 to also output to hub.
        :param ol_pos_dem: 1 for open loop to zero output, 2 to hold output.
        :param x_pos_fb_sense: x gain (-32768 to 32767)
        :param y_pos_fb_sense: x gain (-32768 to 32767)
        :param bay: Index (0-based) of controller bay to send the command.
        :param channel: Index (0-based) of controller bay channel to send the command.
        """
        # write params
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_set_posdemandparams(
                dest=self.bays[bay], source=EndPoint.HOST,
                x_pos_dem_min = x_pos_dem_min,
                y_pos_dem_min = y_pos_dem_min,
                x_pos_dem_max = x_pos_dem_max,
                y_pos_dem_max = y_pos_dem_max,
                lv_out_route = lv_out_route,
                ol_pos_dem = ol_pos_dem,
                x_pos_fb_sense = x_pos_fb_sense,
                y_pos_fb_sense = y_pos_fb_sense,
            )
        )
        # request new params to update the dictionary
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_req_posdemandparams(
                source=EndPoint.HOST, dest=self.bays[bay]
            )
        )


    def set_opermode(self, mode: int, bay=0, channel=0):
        """
        Set the quad detector's operation mode.

        :param mode: 1 for monitor, 2 for open loop, 3 for closed, 4 for auto.
        :param bay: Index (0-based) of controller bay to send the command.
        :param channel: Index (0-based) of controller bay channel to send the command.
        """
        # write mode
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_set_opermode(
                dest=self.bays[bay], source=EndPoint.HOST, mode=mode
            )
        )
        # request new mode to update the dictionary
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_req_opermode(dest=self.bays[bay], source=EndPoint.HOST)
        )


    def set_trigioconfig(self,
        trig1_mode: int,
        trig1_polarity: int,
        trig1_sum_min: int,
        trig1_sum_max: int,
        trig1_diff_threshold: int,
        trig2_mode: int,
        trig2_polarity: int,
        trig2_sum_min: int,
        trig2_sum_max: int,
        trig2_diff_threshold: int,
        bay=0, channel=0,
    ):
        """
        Set the quad detector's trigger config.

        :param trig1_mode: trigger operating mode (see documentation).
        :param trig1_polarity: 1 for active high, 2 for active low.
        :param trig1_sum_min: lower limit to trigger on (1-99)
        :param trig1_sum_max: upper limit to trigger on (1-99)
        :param trig1_diff_threshold: threshold for TRIGOUT_DIFF
        :param trig2_mode: trigger operating mode (see documentation).
        :param trig2_polarity: 1 for active high, 2 for active low.
        :param trig2_sum_min: lower limit to trigger on (1-99)
        :param trig2_sum_max: upper limit to trigger on (1-99)
        :param trig2_diff_threshold: threshold for TRIGOUT_DIFF
        :param bay: Index (0-based) of controller bay to send the command.
        :param channel: Index (0-based) of controller bay channel to send the command.
        """
        # write config
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_set_kpatrigioconfig(
                dest=self.bays[bay], source=EndPoint.HOST,
                trig1_mode = trig1_mode,
                trig1_polarity = trig1_polarity,
                trig1_sum_min = trig1_sum_min,
                trig1_sum_max = trig1_sum_max,
                trig1_diff_threshold = trig1_diff_threshold,
                trig2_mode = trig2_mode,
                trig2_polarity = trig2_polarity,
                trig2_sum_min = trig2_sum_min,
                trig2_sum_max = trig2_sum_max,
                trig2_diff_threshold = trig2_diff_threshold,
            )
        )
        # request new mode to update the dictionary
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_req_kpatrigioconfig(
                dest=self.bays[bay], source=EndPoint.HOST
            )
        )


    def req_readings(self, bay=0, channel=0):
        """
        Request the quad detector's readings.

        :param bay: Index (0-based) of controller bay to send the command.
        :param channel: Index (0-based) of controller bay channel to send the command.
        """
        self._loop.call_soon_threadsafe(
            self._write,
            apt.quad_req_readings(source=EndPoint.HOST, dest=self.bays[bay])
        )


